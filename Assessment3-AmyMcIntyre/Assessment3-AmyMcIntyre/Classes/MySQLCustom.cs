﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "mcinta";
        private static string dbname = "gui_comp6001_16b_assn3";

        //DO NOT CHANGE THESE
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}" });
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }


        //////////////////    DB CLASS ///////////////////////////

        //CHANGE THIS!!
        //This class is specific to your DB
        public class AddPerson
        {
            public int id { get; set; }
            public string fname { get; set; }
            public string lname { get; set; }

            public DateTime dob { get; set; }
            public int strnumber { get; set; }
            public string strname { get; set; }
            public string cityname { get; set; }
            public string postcodenumber { get; set; }
            public string phoneone { get; set; }
            public string phonetwo { get; set; }
            public string emailaddress { get; set; }



            public AddPerson(string _fname, string _lname, DateTime _dob)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(int _id, string _fname, string _lname, DateTime _dob, int _strnumber, string _strname, string _cityname, string _postcodenumber, string _phoneone, string _phonetwo, string _emailaddress)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                strnumber = _strnumber;
                strname = _strname;
                cityname = _cityname;
                postcodenumber = _postcodenumber;
                phoneone = _phoneone;
                phonetwo = _phonetwo;
                emailaddress = _emailaddress;
                id = _id;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _strnumber, string _strname, string _cityname, string _postcodenumber, string _phoneone, string _phonetwo, string _emailaddress)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                strnumber = _strnumber;
                strname = _strname;
                cityname = _cityname;
                postcodenumber = _postcodenumber;
                phoneone = _phoneone;
                phonetwo = _phonetwo;
                emailaddress = _emailaddress;

            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void AddData(string firstnamebox, string lastnamebox, string dateofbirthbox, string streetnumberbox, string addressbox, string townbox, string postcodebox, string mobilebox, string homebox, string emailbox)
        {
            //var IDnum = int.Parse(ID);
            var date = DateTime.Parse(dateofbirthbox);
            var strNumbers = int.Parse(streetnumberbox);

            var customdb = new AddPerson(firstnamebox, lastnamebox, date, strNumbers, addressbox, postcodebox, townbox, mobilebox, homebox, emailbox);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();
                                       //INSERT INTO tbl_people(FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, PHONE1, PHONE2, EMAIL)VALUES('Jeff', 'Test', '1928-10-23', 43, 'fdsdfsa', 1234, 'fdsdfsa', '8765', '8765', '8765');
            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB, STR_NUMBER, STR_NAME, POSTCODE,  CITY, PHONE1, PHONE2, EMAIL)VALUES(@fname, @lname, @dob, @str_number, @str_name, @postcode, @city, @phone1, @phone2, @email)";
            //insertCommand.Parameters.AddWithValue("@id", customdb.id);
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@str_number", customdb.strnumber);
            insertCommand.Parameters.AddWithValue("@str_name", customdb.strname);
            insertCommand.Parameters.AddWithValue("@city", customdb.cityname);
            insertCommand.Parameters.AddWithValue("@postcode", customdb.postcodenumber);
            insertCommand.Parameters.AddWithValue("@phone1", customdb.phoneone);
            insertCommand.Parameters.AddWithValue("@phone2", customdb.phonetwo);
            insertCommand.Parameters.AddWithValue("@email", customdb.emailaddress);
            insertCommand.ExecuteNonQuery();

            con.Clone();

        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void UpdateData(string ID, string firstnamebox, string lastnamebox, string dateofbirthbox, string streetnumberbox, string addressbox, string townbox, string postcodebox, string mobilebox, string homebox, string emailbox)
        {
            var IDnum = int.Parse(ID);
            var date = DateTime.Parse(dateofbirthbox);
            var strNumbers = int.Parse(streetnumberbox);

            var customdb = new AddPerson(IDnum, firstnamebox, lastnamebox, date, strNumbers, addressbox, postcodebox, townbox, mobilebox, homebox, emailbox);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, STR_NUMBER = @str_number, STR_NAME = @str_name, POSTCODE = @postcode, CITY = @city, PHONE1 = @phone1, PHONE2 = @phone2, EMAIL = @email  WHERE ID = @IDnum";
            updateCommand.Parameters.AddWithValue("@IDnum", customdb.id);
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@str_number", customdb.strnumber);
            updateCommand.Parameters.AddWithValue("@str_name", customdb.strname);
            updateCommand.Parameters.AddWithValue("@city", customdb.cityname);
            updateCommand.Parameters.AddWithValue("@postcode", customdb.postcodenumber);
            updateCommand.Parameters.AddWithValue("@phone1", customdb.phoneone);
            updateCommand.Parameters.AddWithValue("@phone2", customdb.phonetwo);
            updateCommand.Parameters.AddWithValue("@email", customdb.emailaddress);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void DeleteDate(string idnum)
        {
            var ID = int.Parse(idnum);

            var customdb = new AddPerson(ID);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

    }
}
