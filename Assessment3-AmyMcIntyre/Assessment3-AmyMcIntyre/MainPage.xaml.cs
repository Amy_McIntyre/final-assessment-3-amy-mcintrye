﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Diagnostics;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;
using System.Data;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment3_AmyMcIntyre
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public string ID;
        public string First_Name;
        public string Last_Name;
        public string Date_Of_Birth;
        public string Street_Number;
        public string Addy;
        public string City;
        public string Post_Code;
        public string Phone1;
        public string Phone2;
        public string Emailaddy;
        


        public MainPage()
        {
            this.InitializeComponent();

            names.ItemsSource = loadAllTheData();

        }
        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            Mysplitview.IsPaneOpen = !Mysplitview.IsPaneOpen;
        }




        private void submitbtn_Click(object sender, RoutedEventArgs e)
        {

            string ID = idnum.Text;
            string First_Name = firstnamebox.Text;
            string Last_Name = lastnamebox.Text;

            string Date_Of_Birth = dateofbirthbox.Text;
            string Street_Number = streetnumberbox.Text;

            string Addy = addressbox.Text;
            string City = townbox.Text;
            string Post_Code = postcodebox.Text;

            string Phone1 = mobilebox.Text;
            string Phone2 = homebox.Text;
            string Emailaddy = emailbox.Text;

            MySQLCustom.AddData(firstnamebox.Text, lastnamebox.Text, dateofbirthbox.Text, streetnumberbox.Text, addressbox.Text, postcodebox.Text, townbox.Text, mobilebox.Text, homebox.Text, emailbox.Text);



            names.ItemsSource = loadAllTheData();
            clearAll();
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        private void update_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.UpdateData(idnum.Text, firstnamebox.Text, lastnamebox.Text, dateofbirthbox.Text, streetnumberbox.Text, addressbox.Text, postcodebox.Text, townbox.Text, mobilebox.Text, homebox.Text, emailbox.Text);
            names.ItemsSource = loadAllTheData();

        }

        private async void delete_Click(object sender, RoutedEventArgs e)
        {
            

            string first = firstnamebox.Text;
            string last = lastnamebox.Text;
            string datebirth = dateofbirthbox.Text;

            MessageDialog outputbox = new MessageDialog($"");
            outputbox.Title = "Are you sure you would like to delete from the database?";

            outputbox.Commands.Add(new Windows.UI.Popups.UICommand($"Yes") { Id = 0 });
            outputbox.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });

            var result = await outputbox.ShowAsync();

            if ((int)result.Id == 0)
            {
                MySQLCustom.DeleteDate(idnum.Text);
            }


        }
        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);
            idnum.Text = b[0];
            firstnamebox.Text = b[1];
            lastnamebox.Text = b[2];
            dateofbirthbox.Text = b[3];
            streetnumberbox.Text = b[4];
            addressbox.Text = b[5];
            postcodebox.Text = b[6];
            townbox.Text = b[7];
            mobilebox.Text = b[8];
            homebox.Text = b[9];
            emailbox.Text = b[10];
        }

        void clearAll()
        {
            firstnamebox.Text = "";
            lastnamebox.Text = "";
            dateofbirthbox.Text = "";
            streetnumberbox.Text = "";
            addressbox.Text = "";
            postcodebox.Text = "";
            townbox.Text = "";
            mobilebox.Text = "";
            homebox.Text = "";
            emailbox.Text = "";
        }

        //Below are the object methods

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }

        private async void MenuButton2_Click(object sender, RoutedEventArgs e)
        {
            string first = firstnamebox.Text;
            string last = lastnamebox.Text;
            string datebirth = dateofbirthbox.Text;

            var command = $"Select concat(ID, ' ',fname, ' ' ,lname) as fullname from tbl_people ";
            var b = MySQLCustom.ShowInList(command);

            MessageDialog outputbox = new MessageDialog($"{string.Join(Environment.NewLine, b)}");
            outputbox.Title = "This is everybody in the database?";

            outputbox.Commands.Add(new Windows.UI.Popups.UICommand($"Done") { Id = 0 });

            var result = await outputbox.ShowAsync();


    }

        private async void MenuButton1_Click(object sender, RoutedEventArgs e)
        {

            MessageDialog outputbox = new MessageDialog("");
            outputbox.Title = "Would you like to reset the application?";

            outputbox.Commands.Add(new Windows.UI.Popups.UICommand($"Yes") { Id = 0 });
            outputbox.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });

            var result = await outputbox.ShowAsync();

            if ((int)result.Id == 0)
            {
                firstnamebox.Text = "";
                lastnamebox.Text = "";
                dateofbirthbox.Text = "";
                streetnumberbox.Text = "";
                addressbox.Text = "";
                postcodebox.Text = "";
                townbox.Text = "";
                mobilebox.Text = "";
                homebox.Text = "";
                emailbox.Text = "";
            }

        }

        private void pic1_Click(object sender, RoutedEventArgs e)
        {

            pic1.NavigateUri = new Uri("https://www.boppoly.ac.nz/");
        }

        private void pic2_Click(object sender, RoutedEventArgs e)
        {
            pic2.NavigateUri = new Uri("https://www.studylink.govt.nz/");
        }

        private void pic3_Click(object sender, RoutedEventArgs e)
        {
            pic3.NavigateUri = new Uri("https://adfs.boppoly.ac.nz/adfs/ls/?SAMLRequest=pVLLTsMwEPyVyPckTtoCstpKpRWiEo%2BKFg5ckGNvqCXHNt4Nr68nTUE8Dr1wsjy7Mzse7xhlY4OYtbR1N%2FDUAlLy2liHoi9MWBud8BINCicbQEFKrGeXF6LMuAjRk1fesh%2BUwwyJCJGMdyxZLibsYTA64SWvBurkmMsa6oqXShUwHGnQFS%2BK6rgYFbXm9YAldxCxY05YJ9TREVtYOiTpqIN4cZQWPC2LDeeCl2IwvGfJonuNcZJ61pYooMhzqWvMKh%2BCt2%2BZVJl776HcYs6S2Ze%2FuXfYNhDXEJ%2BNgtubi2%2BFxnttofwjgqYJFnYRdA26tZCFbcj7O%2B7PMpUKe1RDLVtLKQaWrD5DPDVOG%2Fd4OL9q34TifLNZpavr9YZNxztt0ecRp%2F%2Fx2ABJLUn%2BsTjOfw4Y7xfmqrO2XKy8NeotOfOxkXTY%2BQ4xOq37VhF2f4kEjrrMrfUv8wiSYMIotsDy6X7m772cfgA%3D&RelayState=https%3A%2F%2Fmoodle2.boppoly.ac.nz%2Fauth%2Fsaml%2F&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=CuoDJpnOh6N9S5Io5%2FegvNiHbXYBImy5jOr8lIs21RSPwIgnJbxHiuqcVI36JKVe9PSIdWOWLBm1E7GfjJ9i%2FhnLoJu5YLBSG235OYAMPK036E%2BUbT7YvJWuji5aGFsZH8sZ01nI7EC9kQ4WrC36yyASaiW1gyssknJ0NRKqTlg3ep9XUEj%2F%2FlEsDOmj8aCu%2BwWX5j3Ra2J3mZ5mllgf0iehDvmuAgHyMT8IJYgLP%2B%2FYv7agxZxwfurWX4PZdvo2BOXjQr9ZGGxbCM4DNg4GNmGS%2BKueHg5RJKtFaAXIFZJkkBNCLdSf%2FlK8RjXT9rwUGh07XUswObtxWtMiRKaaDQ%3D%3D");
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            names.ItemsSource = loadAllTheData();
        }
    }
    
    
}
